﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;

// ToDo TileMapCreator improvement :
// show info about selected tile ?
// enable placement of objects larger than 1x1

[CustomEditor(typeof(TileMapCreator))]
public class TileMapCreatorEditor : Editor
{
    private TileMapCreator _tm;

    #region Menu methods

    [MenuItem("Assets/Create/TileSet")]
    public static void CreateTileSet()
    {
        TileSet asset = ScriptableObject.CreateInstance<TileSet>();
        AssetDatabase.CreateAsset(asset, "Assets/TileSet.asset");
        AssetDatabase.SaveAssets();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
        asset.hideFlags = HideFlags.DontSave;
    }

    #endregion

    #region Unity methods

    /// <summary>
    /// Called every time the gameobject is activated or selected in the editor.
    /// </summary>
    void OnEnable()
    {
        _tm = (TileMapCreator)target;

        // preview object
        if (_tm.previewObj == null)
        {
            _tm.previewObj = new GameObject("__previewObject");
            _tm.previewObj.transform.parent = _tm.transform;
            _tm.previewSr = _tm.previewObj.AddComponent<SpriteRenderer>();
            _tm.previewSr.color = new Color(1.0f, 1.0f, 1.0f, 100.0f / 255.0f);
        }
        _tm.previewObj.transform.SetAsFirstSibling(); // make sure the child object is first in the children list
        _tm.previewObj.SetActive(false);

        // layer parents: make sure we have parent objects
        if (_tm.layerParents == null)
        {
            Debug.Log("parents dict is null"); 
            _tm.layerParents = new SelectionDictionary();
        }
        string[] layerNames = GetLayerNames();
        int[] layerValues = GetLayerValues();
        for (int i = 0; i < layerNames.Length; ++i)
        {
            TileMapLayer tml = (TileMapLayer) layerValues[i];
            if (!_tm.layerParents.ContainsKey(tml) || _tm.layerParents[tml] == null)
            {
                Debug.Log("checking for reference, or creating new..." + tml);
                var goName = string.Format("__{0}Layer", layerNames[i].ToLower());

                // check if parent already exists but we lost the reference
                int j = 0;
                bool found = false;
                while (j < _tm.transform.childCount)
                {
                    Transform t = _tm.transform.GetChild(j++);
                    if (t.gameObject.name == goName) // found a match?
                    {
                        Debug.Log("reference found");
                        found = true;
                        _tm.layerParents.Add(tml, t);
                        j = _tm.transform.childCount; // end the loop
                    }
                }

                // create parent for layer if not found
                if (!found)
                    CreateLayerParentObject(tml, goName);
            }
        }

        // Layer tileset selection dict
        if (_tm.layerTileSets == null)
            _tm.layerTileSets = new TilesetDictionary();
    }

    /// <summary>
    /// Called every time the gameobject is deactivated or de-selected (i.e. loses focus) in the editor.
    /// </summary>
    void OnDisable()
    {
        //Debug.Log("TileMapCreatorEditor:OnDisable()");
        _tm.previewObj.SetActive(false);
    }

    public override void OnInspectorGUI()
    {
        SceneView.RepaintAll();

        if (GUILayout.Button("Change Colors"))
        {
            TileMapCreatorColorsWindow window = EditorWindow.GetWindow<TileMapCreatorColorsWindow>();
            window.Init(_tm);
            SceneView.RepaintAll();
        }
        if (GUILayout.Button("Update Connections"))
        {
            UpdatePathfindingConnections();
            SceneView.RepaintAll();
        }

        // Show Grid
        _tm.drawGrid = EditorGUILayout.Toggle("Show Grid", _tm.drawGrid);

        // Show Connections
        _tm.drawConnections = EditorGUILayout.Toggle("Show Connections", _tm.drawConnections);

        // Pathfinding: Corner cuttin
        _tm.cornerCutting = EditorGUILayout.Toggle("Corner Cutting", _tm.cornerCutting);

        // Selected tile
        EditorGUI.BeginChangeCheck();
        var newTilePrefab =
            (Transform) EditorGUILayout.ObjectField("Selected tile", _tm.selectedPrefab, typeof (Transform), false);
        if (EditorGUI.EndChangeCheck())
        {
            _tm.selectedPrefab = newTilePrefab;
            Undo.RecordObject(target, "TileMapCreator Changed"); // = allows the user to undo to revert the change
        }
        //if (_tm.selectedPrefab != null)
        //{ 
        //    Texture2D myTexture = AssetPreview.GetAssetPreview(_tm.selectedPrefab);
        //    //EditorGUI.DrawPreviewTexture(new Rect(0, 0, myTexture.width, myTexture.height), myTexture);
        //    EditorGUILayout.LabelField(new GUIContent(myTexture), new GUIContent("Current selection"));
        //}

        // Current layer
        EditorGUI.BeginChangeCheck();
        var newLayer = (TileMapLayer)EditorGUILayout.IntPopup("Current layer", (int) _tm.currentLayer, GetLayerNames(), GetLayerValues());
        if (EditorGUI.EndChangeCheck())
        {
            SetCurrentLayer(newLayer);
            SetSelectionAndPreviewObject(0);
        }

        // Tileset
        EditorGUILayout.LabelField("Active tilesets");
        string[] layerNames = GetLayerNames();
        int[] layerValues = GetLayerValues();
        for (int layerIndex = 0; layerIndex < layerNames.Length; ++layerIndex)
        {
            TileMapLayer layerEnum = (TileMapLayer) layerValues[layerIndex];
            TileSet currentTileSet = _tm.layerTileSets.ContainsKey(layerEnum) ? _tm.layerTileSets[layerEnum] : null;
            EditorGUI.BeginChangeCheck();
            var newTileSet = (TileSet)EditorGUILayout.ObjectField(layerNames[layerIndex], currentTileSet, typeof(TileSet), false);
            if (EditorGUI.EndChangeCheck())
            {
                if (_tm.layerTileSets.ContainsKey(layerEnum))
                    _tm.layerTileSets[layerEnum] = newTileSet;
                else
                    _tm.layerTileSets.Add(layerEnum, newTileSet);

                _tm.currentTileSet = newTileSet;
            }
        }

        // Selection
        if (_tm.currentTileSet != null)
        {
            var names = new string[_tm.currentTileSet.prefabs.Count];
            var values = new int[_tm.currentTileSet.prefabs.Count];

            for (int i = 0; i < names.Length; ++i)
            {
                names[i] = _tm.currentTileSet.prefabs[i] != null ? _tm.currentTileSet.prefabs[i].name : "";
                values[i] = i;
            }

            EditorGUI.BeginChangeCheck();
            var index = EditorGUILayout.IntPopup("Select tile", _tm.oldIndex, names, values);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(target, "TileMapCreator Changed");
                if (_tm.oldIndex != index)
                {
                    SetSelectionAndPreviewObject(index);
                }
            }
        }
    }

    void OnSceneGUI()
    {
        int controlId = GUIUtility.GetControlID(FocusType.Passive);
        Event e = Event.current;

        // activate, move and colorize preview object
        if (_tm.previewSr != null && _tm.selectedPrefab != null)
        {
            _tm.previewObj.SetActive(true);
            _tm.previewObj.transform.position = GetAlignedPosition(e);
            _tm.previewSr.color = CanPlaceTile(e) ? _tm.canPlaceColor : _tm.cannotPlaceColor;
        }
        else
            _tm.previewObj.SetActive(false);

        if (e.isMouse && (e.type == EventType.MouseDown || e.type == EventType.MouseDrag) && e.button == 0)
        {
            GUIUtility.hotControl = controlId;
            e.Use();
            GameObject gameObject;
            Transform prefab = _tm.selectedPrefab;
            if (prefab != null && CanPlaceTile(e))
            {
                Undo.IncrementCurrentGroup();
                gameObject = PlaceTile(e);
                Undo.RegisterCreatedObjectUndo(gameObject, "Create " + gameObject.name);
            }
        }
        else if (e.isMouse && e.type == EventType.MouseDown && e.button == 1)
        {
            GUIUtility.hotControl = controlId;
            e.Use();
            DeleteTile(e);
        }
        else if (e.isMouse && e.type == EventType.MouseUp && (e.button == 0 || e.button == 1))
        {
            GUIUtility.hotControl = 0;
        }
    }

    #endregion Unity methods

    private void UpdatePathfindingConnections()
    {
        // Step 1: Clear current connections
        foreach (Transform child in _tm.transform)
        {
            var node = child.GetComponent<TileNode>();
            if (node != null)
            {
                node.ClearConnections();
                EditorUtility.SetDirty(node);
            }
        }
        // Step 2: Calculate new connections
        int i = 0;
        while (i < _tm.transform.childCount)
        {
            Transform t = _tm.transform.GetChild(i++);
            TileNode node = t.GetComponent<TileNode>();
            if (node == null || !node.isWalkable)
                continue; // we don't wanna create connections with unwalkable tiles
            TileNode nodeAbove = GetTileNodeAtCoordinate(node.xCoord, node.yCoord + 1);
            TileNode nodeDiagonalUp = GetTileNodeAtCoordinate(node.X + 1, node.Y + 1);
            TileNode nodeRight = GetTileNodeAtCoordinate(node.X + 1, node.Y);
            TileNode nodeDiagonalDown = GetTileNodeAtCoordinate(node.X + 1, node.Y - 1);
            TileNode nodeBelow = GetTileNodeAtCoordinate(node.X, node.Y - 1);
            if (nodeAbove != null && nodeAbove.isWalkable)
            {
                //nodeAbove.Connections.Add(node); = WRONG/BAD!
                node.AddConnection(nodeAbove);
                nodeAbove.AddConnection(node);
            }
            if (nodeDiagonalUp != null && nodeDiagonalUp.isWalkable && nodeAbove != null && nodeRight != null)
            {
                if ((_tm.cornerCutting && (nodeAbove.isWalkable || nodeRight.isWalkable)) ||
                    (!_tm.cornerCutting && (nodeAbove.isWalkable && nodeRight.isWalkable)))
                {
                    node.AddConnection(nodeDiagonalUp);
                    nodeDiagonalUp.AddConnection(node);
                }
            }
            if (nodeRight != null && nodeRight.isWalkable)
            {
                node.AddConnection(nodeRight);
                nodeRight.AddConnection(node);
            }
            if (nodeDiagonalDown != null && nodeDiagonalDown.isWalkable && nodeRight != null && nodeBelow != null)
            {
                if ((_tm.cornerCutting && (nodeRight.isWalkable || nodeBelow.isWalkable)) ||
                    (!_tm.cornerCutting && (nodeRight.isWalkable && nodeBelow.isWalkable)))
                {
                    node.AddConnection(nodeDiagonalDown);
                    nodeDiagonalDown.AddConnection(node);
                }
            }


            // NOT NECESSARY
            //if (nodeBelow.isWalkable)
            //{
            //    node.AddConnection(nodeBelow);
            //    nodeBelow.AddConnection(node);
            //}
        } // end of while
        // Step 3: Save
        foreach (Transform child in _tm.transform)
        {
            var node = child.GetComponent<TileNode>();
            EditorUtility.SetDirty(node);
            //EditorUtility.SetDirty(nodeAbove);
            //EditorUtility.SetDirty(nodeDiagonalUp);
            //EditorUtility.SetDirty(nodeRight);
            //EditorUtility.SetDirty(nodeDiagonalUp);
            //EditorUtility.SetDirty(nodeBelow);
        }
        Debug.Log("[TileMapCreator]: Connections updated.");
    }

    private float CreateSlider(string labelName, float sliderPosition)
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label(labelName);
        sliderPosition = EditorGUILayout.Slider(sliderPosition, 1f, 100f);
        GUILayout.EndHorizontal();
        return sliderPosition;
    }

    private void SetCurrentLayer(TileMapLayer newLayer)
    {
        _tm.currentLayer = newLayer;
        _tm.currentTileSet = GetTileSetForLayer(newLayer);
    }

    private TileSet GetTileSetForLayer(TileMapLayer layer)
    {
        return _tm.layerTileSets.ContainsKey(layer) ? _tm.layerTileSets[layer] : null;
    }

    private void SetSelectionAndPreviewObject(int index)
    {
        _tm.oldIndex = index;
        _tm.selectedPrefab = _tm.currentTileSet != null && _tm.currentTileSet.prefabs != null &&
                             _tm.currentTileSet.prefabs.Count > index
                                ? _tm.currentTileSet.prefabs[index].transform
                                : null;

        // set preview obj
        if (_tm.selectedPrefab != null)
        { 
            var sr = _tm.selectedPrefab.GetComponent<SpriteRenderer>();
            _tm.previewSr.sprite = sr.sprite;
            _tm.previewSr.flipX = sr.flipX;
            _tm.previewSr.flipY = sr.flipY;


            // set grid size
            _tm.GRID_WIDTH = _tm.selectedPrefab.GetComponent<Renderer>().bounds.size.x;
            _tm.GRID_HEIGHT = _tm.selectedPrefab.GetComponent<Renderer>().bounds.size.y;
            SceneView.RepaintAll();
        }
    }

    private GameObject CreateLayerParentObject(TileMapLayer layer, string goName)
    {
        var go = new GameObject();
        go.transform.parent = _tm.transform;
        go.name = goName;
        _tm.layerParents.Add(layer, go.transform);
        return null;
    }

    private bool CanPlaceTile(Event e)
    {
        TileCoord coord = GetCoordinatesFromMousePosition(e);
        TileNode node = GetTileNodeAtCoordinate(coord);
        return node == null;
    }

    private GameObject PlaceTile(Event e)
    {
        GameObject go = (GameObject)PrefabUtility.InstantiatePrefab(_tm.selectedPrefab.gameObject);
        Vector3 aligned = GetAlignedPosition(e);
        go.transform.position = aligned;
        go.transform.parent = GetParentForCurrentLayer();
        TileCoord coord = GetCoordinatesFromMousePosition(e);
        var tileNode = go.GetComponent<TileNode>();
        go.name += string.Format("[{0},{1}]", coord.X, coord.Y);
        tileNode.SetCoords(coord);
        return go;
    }

    private Transform GetParentForCurrentLayer()
    {
        return _tm.layerParents[_tm.currentLayer]; // should always be populated
    }

    private void DeleteTile(Event e)
    {
        Vector3 aligned = GetAlignedPosition(e);
        Transform tt = GetTileNodeTransformFromPosition(aligned);
        if (tt != null)
        {
            DestroyImmediate(tt.gameObject);
        }
    }

    private TileNode GetTileNodeAtCoordinate(TileCoord coord)
    {
        return GetTileNodeAtCoordinate(coord.X, coord.Y);
    }

    private TileNode GetTileNodeAtCoordinate(int x, int y)
    {
        int i = 0;
        Transform layerParent = GetParentForCurrentLayer();
        while (i < layerParent.childCount)
        {
            Transform t = layerParent.GetChild(i++);
            var tileNode = t.GetComponent<TileNode>();
            if (tileNode != null && tileNode.X == x && tileNode.Y == y)
                return tileNode;
        }
        return null;
    }

    private Vector3 GetAlignedPosition(Event e)
    {
        Ray ray =
            Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x,
                -e.mousePosition.y + Camera.current.pixelHeight, 0));
        Vector3 mousePos = ray.origin;
        return new Vector3(Mathf.Floor(mousePos.x/_tm.GRID_WIDTH)*_tm.GRID_WIDTH + _tm.GRID_WIDTH/2.0f,
            Mathf.Floor(mousePos.y/_tm.GRID_HEIGHT)*_tm.GRID_HEIGHT + _tm.GRID_HEIGHT/2.0f, 0.0f);
    }

    private TileCoord GetCoordinatesFromMousePosition(Event e)
    {
        Ray ray =
            Camera.current.ScreenPointToRay(new Vector3(e.mousePosition.x,
                -e.mousePosition.y + Camera.current.pixelHeight, 0));
        Vector3 mousePos = ray.origin;
        return new TileCoord(Mathf.FloorToInt(mousePos.x / _tm.GRID_WIDTH), Mathf.FloorToInt(mousePos.y / _tm.GRID_HEIGHT));
    }

    /// <summary>
    /// Finds a transform that has a TileNode component on it on the specified position, and returns it.
    /// </summary>
    /// <param name="aligned">The aligned position.</param>
    /// <returns>The transform for the gameobject at that position.</returns>
    private Transform GetTileNodeTransformFromPosition(Vector3 aligned)
    {
        int i = 0;
        Transform layerParent = GetParentForCurrentLayer();
        while (i < layerParent.childCount)
        {
            Transform t = layerParent.GetChild(i++);
            if (t.position == aligned && t.GetComponent<TileNode>() != null) // check for TileNode component
            {
                return t;
            }
        }
        return null;
    }

    private string[] GetLayerNames()
    {
        return Enum.GetNames(typeof (TileMapLayer));
    }

    private int[] GetLayerValues()
    {
        Array arr = Enum.GetValues(typeof (TileMapLayer));
        int[] intArr = new int[arr.Length];
        for (int i = 0; i < arr.Length; ++i)
            intArr[i] = (int) arr.GetValue(i);
        return intArr;
    }
}
