﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour 
{
	public GameObject mainMenuPanel;
	public GameObject instructionsPanel;
	
	public void StartNewGame()
	{
        SceneManager.LoadScene("MAIN_MENU");
    }
	
	public void ShowInstructions()
	{
		mainMenuPanel.SetActive(false);
		instructionsPanel.SetActive(true);
	}
	
	public void BackToMainMenu()
	{
		mainMenuPanel.SetActive(true);
		instructionsPanel.SetActive(false);
	}
	
	public void QuitApplication()
	{
		Application.Quit();
	}
}
