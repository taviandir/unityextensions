﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public abstract class BaseBehaviour : MonoBehaviour
{
    public bool debug = false;

    protected void Log(object message)
    {
        if (!debug)
            return;

        string realMessage = string.Format("[{0}]: {1}", this.GetType().Name, message);
        if (Application.isPlaying)
            GConsole.Log(realMessage);
        else
            Debug.Log(realMessage);
    }
}
