﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

[Serializable] public class SelectionDictionary : SerializableDictionary<TileMapLayer, Transform> { }
[Serializable] public class TilesetDictionary : SerializableDictionary<TileMapLayer, TileSet> { }

public class TileMapCreator : MonoBehaviour
{
    public bool drawGrid = true;
    public bool drawConnections = true;
    public bool cornerCutting = false; // for pathfinding
    public float GRID_WIDTH = 32.0f;
    public float GRID_HEIGHT = 32.0f;

    // COLORS
    public Color gridColor = Color.green;
    public Color connectionColor = Color.red;
    public Color canPlaceColor = new Color(0f, 1f, 0f, 150.0f/255.0f);
    public Color cannotPlaceColor = new Color(1f, 0f, 0f, 150.0f/255.0f);

    // SELECTION
    public Transform selectedPrefab;
    public TileSet currentTileSet;
    public GameObject previewObj;
    public SpriteRenderer previewSr;
    public int oldIndex = 0;
    public TilesetDictionary layerTileSets;

    // LAYERS
    public TileMapLayer currentLayer;
    public SelectionDictionary layerParents;

    private readonly AStarManhattanTileMaker _pathfinder = new AStarManhattanTileMaker();

    void Start()
    {
        // make sure the preview tile is not visible during gameplay/runtime
        if (previewObj != null)
            previewObj.SetActive(false);
    }

    void OnDrawGizmos()
    {
        Vector3 pos = Camera.current.transform.position;

        if (drawGrid)
        {
            Gizmos.color = gridColor;
            // vertical lines
            for (float y = pos.y - 800.0f; y < pos.y + 800.0f; y += GRID_HEIGHT)
            {
                Gizmos.DrawLine(new Vector3(-100000f, Mathf.Floor(y/GRID_HEIGHT)*GRID_HEIGHT, 0),
                    new Vector3(100000f, Mathf.Floor(y/GRID_HEIGHT)*GRID_HEIGHT, 0));
            }
            // horizontal lines
            for (float x = pos.x - 1200.0f; x < pos.x + 1200.0f; x += GRID_WIDTH)
            {
                Gizmos.DrawLine(new Vector3(Mathf.Floor(x/GRID_WIDTH)*GRID_WIDTH, -100000f, 0),
                    new Vector3(Mathf.Floor(x/GRID_WIDTH)*GRID_WIDTH, 100000f, 0));
            }
        }
        if (drawConnections)
        {
            Gizmos.color = connectionColor;
            int i = 0;
            while (i < transform.childCount)
            {
                Transform t = transform.GetChild(i++);
                TileNode node = t.GetComponent<TileNode>();
                if (node == null || node.Connections == null) continue;
                foreach (TileNode conn in node.Connections)
                {
                    if (conn == null)
                    {
                        Debug.Log("connection null:" + node.xCoord + "," + node.yCoord);
                        continue;
                    }
                    if (conn.X > node.X || conn.Y > node.Y)
                    {
                        Gizmos.DrawLine(node.transform.position, conn.transform.position);
                    }
                }
            }
        }
    }

    public IEnumerable<TileNode> CalculatePath(TileNode from, TileNode to)
    {
        return _pathfinder.FindPathBetween(from, to);
    }
}

public enum TileMapLayer
{
    Background = 0,
    Objects = 1,
}
