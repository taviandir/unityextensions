﻿/*

Based on the platform controller tutorial series by Sebastian Lague
URL: https://www.youtube.com/watch?v=MbWK8bCAU2w&list=PLFt_AvWsXl0f0hqURlhyIoAabKPgRsqjz

*/

using UnityEngine;
using System.Collections;

[RequireComponent (typeof (PlatformerController2D))]
public class PlatformerPlayer : MonoBehaviour
{
    public bool hasControl = true;
    public bool wallSlidingEnabled = false;
    public bool doubleJumpEnabled = false;
	public float moveSpeed = 4f;
	public float minJumpHeight = 1f;
	public float maxJumpHeight = 4f;
	public float timeToJumpApex = .4f;
	public Vector2 wallJumpClimb = new Vector2(7.5f, 16f);
	public Vector2 wallJumpOff = new Vector2(8.5f, 7f);
	public Vector2 wallLeap = new Vector2(18f, 17f);
	public float wallStickTime = 0.25f;
	
	private float timeToWallUnstick;
	private float accelerationTimeAirborne = .2f;
	private float accelerationTimeGrounded = .1f;
	private float wallSlideSpeedMax = 2.5f;
	private float gravity;
	private float maxJumpVelocity;
	private float minJumpVelocity;
	private Vector3 velocity;
	private float velocityXSmoothing;
    private bool hasDoubleJumped = false;

	private PlatformerController2D controller;
    private SpriteRenderer sr;

	void Start() 
	{
		controller = GetComponent<PlatformerController2D>();
	    sr = GetComponent<SpriteRenderer>();

		gravity = -(2 * maxJumpHeight) / Mathf.Pow (timeToJumpApex, 2);
		maxJumpVelocity = Mathf.Abs(gravity) * timeToJumpApex;
		minJumpVelocity = Mathf.Sqrt(2*Mathf.Abs(gravity) * minJumpHeight);
	}

	void Update()
	{
        // get input
		Vector2 input = hasControl ? new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")) : Vector2.zero;

        // flip sprite depending on direction
        sr.flipX = input.x < 0 ? true : input.x > 0 ? false : sr.flipX;
        //sr.flipX = controller.collisions.faceDir == -1;

        float targetVelocityX = input.x * moveSpeed;
		velocity.x = Mathf.SmoothDamp(velocity.x, targetVelocityX, ref velocityXSmoothing, (controller.collisions.below)?accelerationTimeGrounded:accelerationTimeAirborne);
		
		int wallDirX = (controller.collisions.left) ? -1 : 1;
		bool wallSliding = false;

	    if (controller.collisions.below)
	        hasDoubleJumped = false;
		
		if (wallSlidingEnabled && (controller.collisions.left || controller.collisions.right) && !controller.collisions.below && velocity.y < 0)
		{
			wallSliding = true;
		    hasDoubleJumped = false;
			if (velocity.y < -wallSlideSpeedMax)
			{
				velocity.y = -wallSlideSpeedMax;
			}
			if (timeToWallUnstick > 0)
			{
				velocityXSmoothing = 0;
				velocity.x = 0;
				
				if (input.x != wallDirX && input.x != 0)
				{
					timeToWallUnstick -= Time.deltaTime;
				}
				else
				{
					timeToWallUnstick = wallStickTime;
				}
			}
			else
			{
				timeToWallUnstick = wallStickTime;
			}
		}

		if (Input.GetKeyDown(KeyCode.Space))
        {
            // wall jump
			if (wallSlidingEnabled && wallSliding)
			{
				if (wallDirX == input.x)
				{
					velocity.x = -wallDirX * wallJumpClimb.x;
					velocity.y = wallJumpClimb.y;
				}
				else if (input.x == 0)
				{
					velocity.x = -wallDirX * wallJumpOff.x;
					velocity.y = wallJumpOff.y;
				}
				else
				{
					velocity.x = -wallDirX * wallLeap.x;
					velocity.y = wallLeap.y;
				}
			}
            // normal jump
			if (controller.collisions.below)
			{
				velocity.y = maxJumpVelocity;
			}
            // in-air jump
            else if (doubleJumpEnabled && !controller.collisions.above && !hasDoubleJumped)
            {
                hasDoubleJumped = true;
                velocity.y = maxJumpVelocity * 0.75f;
            }
		}
		if (Input.GetKeyUp(KeyCode.Space) && !hasDoubleJumped)
		{
			if (velocity.y > minJumpVelocity)
				velocity.y = minJumpVelocity;
		}

		velocity.y += gravity * Time.deltaTime;
		controller.Move(velocity * Time.deltaTime, input);
		
		if (controller.collisions.above || controller.collisions.below)
        {
			velocity.y = 0;
		}
	}

    public void ToggleControl(bool value)
    {
        hasControl = value;
    }

    public void Respawn()
    {
        transform.position = new Vector3(0.67f, -0.2f, 0f); // ToDo : proper respawn
    }
}